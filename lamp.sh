#!/bin/bash
# install httpd
 
sudo apt update -y
sudo apt install apache2 -y
sudo systemctl start apache2
sudo systemctl enable apache2
sudo apt install php libapache2-mod-php php-mysql php-cli -y
sudo systemctl restart apache2
 
sudo apt install -y mysql-server
sudo systemctl start mysql.service
sudo systemctl enable mysql.service
